package notes;

public class AudioNote extends AttachmentNote {

	public AudioNote() {
		super();
		id = new StringBuilder("vc-").append(numberOfNotes).append("-")
				.append(timestamp.substring(0, timestamp.indexOf(",")));
	}

	@Override
	public void printNote() {
		super.printNote();
		System.out.printf("Attatched voice message from address \"%s\".\n", getAddress());
		System.out.println("[VOICE MESSAGE GOES HERE]");

	}
}
