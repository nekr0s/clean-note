package notes;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

import interfaces.Notifiable;
import interfaces.Uploadable;

// generate ID depending on the kind of note -> TXT_00126062018, IMG_00126062018, VC_00126062018

public class Note implements Notifiable, Uploadable {
	// Every note has a scanner and a KEYWORD
	Scanner in = new Scanner(System.in);
	String keyword = "end";
	// Every note has a scanner and a KEYWORD

	// Format of the TIMESTAMP
	public static DateFormat dateFormat = new SimpleDateFormat("ddMMyyyy,HH:mm");
	// Format of the TIMESTAMP

	protected static int numberOfNotes = 0;
	protected static int longestParagraph = 0;
	ArrayList<Paragraph> note;
	protected StringBuilder id;

	protected Date currentDate;
	protected String timestamp;
	private String time;

	public Note() {
		note = new ArrayList<>();
		// if note is empty throw exception

		addToNote();

		currentDate = new Date();
		timestamp = dateFormat.format(currentDate);
		numberOfNotes++;

		id = new StringBuilder("txt-").append(numberOfNotes).append("-")
				.append(timestamp.substring(0, timestamp.indexOf(",")));
		time = timestamp.substring(timestamp.indexOf(",") + 1);
	}

	// need to add "empty note can't be saved"
	public void addToNote() {
		System.out.println("Enter text, \"end\" to stop");
		String text = in.nextLine();
		while (!text.toLowerCase().equals(keyword)) {
			Paragraph p = new Paragraph(text);
			longestParagraph = Math.max(longestParagraph, p.getPara().length());
			note.add(p);
			text = in.nextLine();

		}
	}

	// print the note
	public void printNote() {
		System.out.printf("\n[%s]\n", getId());
		System.out.println(genSequence(longestParagraph, '-'));
		// System.out.printf("[%s]\n\n", getId());
		for (Paragraph para : note) {
			para.printPara();
		}
		System.out.println(genSequence(longestParagraph, '_'));
	}

	// gen sequence for end or start
	public String genSequence(int len, char symb) {
		StringBuilder seq = new StringBuilder(new String(new char[len]).replace('\0', symb));
		seq.insert(0, ".");
		seq.append(".");
		return seq.toString();
	}

	public void uploadToCloud() {
		System.out.println("Successfully uploaded to cloud!");
	}

	public void notiffy() {
		System.out.println(
				"1.Notify by email \n 2.Notify by SMS \n 3.Notify with phone's notification \n 4.Notify by each of the above");

	}
	// gets note at index

	public ArrayList<Paragraph> getFinalNote() {
		return note;
	}

	public String getId() {
		return id.toString();
	}

	public String getTime() {
		return time;
	}

	public boolean isEmpty() {
		int checker = 0;
		for (Paragraph p : note) {
			if (!p.getPara().isEmpty() && !p.getPara().equals(" ") && !p.getPara().equals(""))
				checker = 1;
		}

		if (checker == 1)
			return false;
		System.out.println("Blank note not added.");
		return true;
	}
}
