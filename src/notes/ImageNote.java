package notes;

public class ImageNote extends AttachmentNote {

	public ImageNote() {
		super();
		id = new StringBuilder("img-").append(numberOfNotes).append("-")
				.append(timestamp.substring(0, timestamp.indexOf(",")));
	}

	@Override
	public void printNote() {
		super.printNote();
		System.out.printf("Displaying image from address \"%s\".\n", getAddress());
		System.out.println("[IMAGE GOES HERE]");

	}

}
