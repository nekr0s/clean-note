package notes;

import interfaces.Colorable;

// The class Paragraph is used to create paragraphs with different starter symbols,
// example - to-do list would start with [ ] -> [COMPLETE], [!] for URGENT, [*] for important etc.
public class Paragraph implements Colorable {
	private String starter;
	private String text;

	public static final String ANSI_RESET = "\u001b[0m";
	public static final String ANSI_RED = "\u001b[1;31m";
	public static final String ANSI_GREEN = "\u001b[1;32m";
	public static final String ANSI_YELLOW = "\u001b[1;33m";

	// Constructors
	// Newline paragraph
	public Paragraph() {
		starter = "";
		text = "";
	}

	// Paragraph without starter symbol
	public Paragraph(String initText) {
		starter = "";
		text = initText;
	}

	// Paragraph with starter symbol
	public Paragraph(StarterSymbol initStarter, String initText) {
		starter = initStarter.toString();
		text = initText;
	}

	public void printPara() {
		System.out.println(" " + getPara());

	}

	public String getPara() {
		if (starter == "")
			return text;
		return starter + " " + text;
	}

	// getters/setters for starter symbol
	public String getStarter() {
		return starter;
	}

	public void setStarter(StarterSymbol newStarter) {
		starter = newStarter.toString();
	}

	@Override
	public String color(int priority) {
		if (priority == 1) {
			return ANSI_RED + getPara() + ANSI_RESET;
		} else if (priority == 2) {
			return ANSI_YELLOW + getPara() + ANSI_RESET;
		} else if (priority == 3) {
			return ANSI_GREEN + getPara() + ANSI_RESET;
		} else {
			return new String("Invalid");
		}
	}

	@Override
	public String color(String seq) {
		System.out.println("That method is for the note, not paragraph!");
		return null;
	}

}
