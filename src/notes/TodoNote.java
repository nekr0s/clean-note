package notes;

public class TodoNote extends Note {

	public TodoNote() {
		super();
		// addToNote();
		id = new StringBuilder("todo-").append(numberOfNotes).append("-")
				.append(timestamp.substring(0, timestamp.indexOf(",")));
	}

	// create to-do list note
	@Override
	public void addToNote() {
		System.out.println("Enter text, \"end\" to stop.");
		String text = in.nextLine();
		while (!text.equals(keyword)) {
			Paragraph p = new Paragraph(StarterSymbol.TODO_NEW, text);
			longestParagraph = Math.max(longestParagraph, p.getPara().length());
			note.add(p);
			text = in.nextLine();
		}
	}

	// mark to-do as COMPLETE
	// int pos -> which element of the to-do list, starting from 1, not 0.
	public void markTodoComplete(int pos) {
		getFinalNote().get(pos - 1).setStarter(StarterSymbol.TODO_COMPLETE);
	}

}
