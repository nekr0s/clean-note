package notes;

// This file is coded as UTF-8 !
// Make sure your console is configured to output UTF-8 encoding!
public enum StarterSymbol {
    TODO_NEW("[ ]"), TODO_COMPLETE("[✓]"), URGENT("[!]"), IMPORTANT("[*]"), LIST("-"), DOT(".");

    private String value;

    private StarterSymbol(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return this.value;
    }
}

