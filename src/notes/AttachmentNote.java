package notes;

import interfaces.Attachable;

public class AttachmentNote extends Note implements Attachable {

	private String address;

	public AttachmentNote() {
		super();

	}

	@Override
	public void addToNote() {
		System.out.println("Import from URL or LOCAL?");
		String input = in.nextLine();
		if (input.toUpperCase().equals("URL")) {
			getFromURL();
		} else if (input.toUpperCase().equals("LOCAL")) {
			getFromLocal();
		} else {
			System.out.println("Invalid");
			return;
		}

		System.out.println("Add text? y/n");
		input = in.nextLine();
		if (input.equals("y")) {
			super.addToNote();
		} else {
			return;
		}

	}

	@Override
	public void getFromURL() {
		System.out.println("Enter URL address: ");
		while (true) {
			try {
				setAddress(in.nextLine());
				return;
			} catch (Exception e) {
				System.out.println("Invalid address, try again.");
				// e.printStackTrace();
			}
		}

	}

	@Override
	public void getFromLocal() {
		System.out.println("Enter LOCAL address: ");
		while (true) {
			try {
				setAddress(in.nextLine());
				return;
			} catch (Exception e) {
				System.out.println("Invalid address, try again.");
				// e.printStackTrace();
			}
		}

	}

	// getters and setters

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) throws Exception {

		if (address.isEmpty() || !address.contains(".")) {
			throw new Exception("Invalid address, try again.");
		}

		this.address = address;
	}

	@Override
	public void printNote() {
		if (note.isEmpty()) {
			System.out.printf("[%s]\n", getId());
		} else {
			super.printNote();
		}
	}

	@Override
	public boolean isEmpty() {
		if (address.isEmpty())
			return super.isEmpty();
		else
			return false;
	}
}
