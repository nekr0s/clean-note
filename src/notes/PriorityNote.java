package notes;

import interfaces.Colorable;

public class PriorityNote extends Note implements Colorable {
	private int priority;

	public PriorityNote() {
		super();
		id = new StringBuilder("pri-").append(numberOfNotes).append("-")
				.append(timestamp.substring(0, timestamp.indexOf(",")));
		setPriority();
	}

	// getters and setters

	public int getPriority() {
		return priority;
	}

	public void setPriority() {
		System.out.println("1 - TOP Priority, 3 - LOWEST Priority.");
		System.out.println("Set priority for the note (from 1 to 3): ");
		int priority = in.nextInt();
		if (priority > 0 && priority < 4) {
			this.priority = priority;
		} else {
			System.out.println("Invalid input");
			return;
		}

	}

	// behave
	@Override
	public String color(String seq) {
		if (priority == 1)
			return "\u001b[31m" + seq + "\u001b[0m";
		else if (priority == 2)
			return "\u001b[33m" + seq + "\u001b[0m";
		else if (priority == 3)
			return "\u001b[32m" + seq + "\u001b[0m";
		else
			return new String("Error");
	}

	@Override
	public void printNote() {
		System.out.printf("\n\u001b[1;96m[%s]\u001b[0m\n", getId());
		System.out.println(color(genSequence(longestParagraph, '-')));
		for (Paragraph para : note) {
			System.out.println(" " + para.color(priority));
		}
		System.out.println(color(genSequence(longestParagraph, '_')));
	}

	@Override
	public String color(int priority) {
		System.out.println("That method is for the paragraph, not note!");
		return null;
	}

}
