package notes;

public class DocumentNote extends AttachmentNote {

	public DocumentNote() {
		super();
		id = new StringBuilder("doc-").append(numberOfNotes).append("-")
				.append(timestamp.substring(0, timestamp.indexOf(",")));
	}

	@Override
	public void printNote() {
		super.printNote();
		System.out.printf("Attached document from address \"%s\".\n", getAddress());
		System.out.println("[DOCUMENT GOES HERE]");
	}
}
