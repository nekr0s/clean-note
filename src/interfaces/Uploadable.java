package interfaces;

public interface Uploadable {

     void uploadToCloud();
}
