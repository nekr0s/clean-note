
package interfaces;

public interface Attachable {

    void getFromURL();

    void getFromLocal();
}

