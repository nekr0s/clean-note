package interfaces;

public interface Colorable {

	String color(String seq);

	String color(int priority);
}
