package boards;

import boards.base.Board;
import notes.Note;

public class Archive extends Board {

	public Archive(String name) {
		super(name);
	}

	// behave
	@Override
	public void addToBoard(Note n) {
		notesInBoard.add(n);
	}

}
