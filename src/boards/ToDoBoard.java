package boards;

import boards.base.Board;
import notes.Note;
import notes.TodoNote;

public class ToDoBoard extends Board {

	// private ArrayList<TodoNote> allTodoNotes;

	public ToDoBoard(String name) {
		super(name);
	}

	// behave

	@Override
	public void addToBoard(Note n) {
		if (n instanceof TodoNote)
			notesInBoard.add(n);

	}

}
