package boards;

import boards.base.Board;
import interfaces.Attachable;
import notes.AudioNote;
import notes.DocumentNote;
import notes.ImageNote;
import notes.Note;

public class AttachmentBoard extends Board {

	public AttachmentBoard(String name) {
		super(name);
	}

	// behave

	@Override
	public void addToBoard(Note n) {
		if (n instanceof Attachable)
			notesInBoard.add(n);
	}

	public void printImageNotesInBoard() {
		for (Note n : notesInBoard) {
			if (n instanceof ImageNote) {
				n.printNote();
			}
		}
	}

	public void printDocNotesInBoard() {
		for (Note n : notesInBoard) {
			if (n instanceof DocumentNote) {
				n.printNote();
			}
		}
	}

	public void printVoiceNotesInBoard() {
		for (Note n : notesInBoard) {
			if (n instanceof AudioNote) {
				n.printNote();
			}
		}
	}

}
