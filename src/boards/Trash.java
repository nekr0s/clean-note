package boards;

import boards.base.Board;
import notes.Note;

public class Trash extends Board {

	public Trash(String name) {
		super(name);
	}

	@Override
	public void addToBoard(Note n) {
		notesInBoard.add(n);
	}

	@Override
	public void printBoard() {
		super.printBoard();
		System.out.println("All notes get deleted in 30 days.");
	}

}
