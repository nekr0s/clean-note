package boards.base;

import java.util.ArrayList;

import interfaces.Notifiable;
import interfaces.Uploadable;
import notes.Note;

public class Board implements Notifiable, Uploadable {

	protected ArrayList<Note> notesInBoard;
	protected String name;

	public Board(String name) {
		notesInBoard = new ArrayList<Note>();
		setName(name);
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public ArrayList<Note> getNotesInBoard() {
		return notesInBoard;
	}

	public void printBoard() {
		System.out.printf("This is the %s!\n", name);
		if (notesInBoard.isEmpty()) {
			System.out.println("[board is empty]");
			return;
		}
		for (Note n : notesInBoard)
			n.printNote();
	}

	// move from one board to another

	public void move(Note n, Board[] boards, Board newHome) {
		// searches all instances and removes all from old home
		for (int i = 0; i < boards.length; i++) {
			if (boards[i].notesInBoard.contains(n))
				boards[i].notesInBoard.remove(notesInBoard.indexOf(n));
		}
		// adds it to its new home
		newHome.notesInBoard.add(n);
		System.out.printf("Note with id %s is moved from %s to %s!\n", n.getId(), name, newHome.name);

	}

	public void addToBoard(Note n) {
		if (n.getId().startsWith("txt"))
			notesInBoard.add(n);
	}

	public void emptyBoard() {
		notesInBoard.clear();
	}

	public Note getById(String id) {
		for (Note n : notesInBoard) {
			if (id.equals(n.getId())) {
				return n;
			}
		}
		return null;
	}

	public void uploadToCloud() {
		System.out.println("Successfully uploaded to cloud!");
	}

	public void notiffy() {
		System.out.println(
				"1.Notify by email \n 2.Notify by SMS \n 3.Notify with phone's notification \n 4.Notify by each of the above");

	}

}
