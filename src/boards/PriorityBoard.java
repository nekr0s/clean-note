package boards;

import boards.base.Board;
import notes.Note;
import notes.PriorityNote;

public class PriorityBoard extends Board {

	public PriorityBoard(String name) {
		super(name);
	}

	@Override
	public void addToBoard(Note n) {
		if (n instanceof PriorityNote) {
			notesInBoard.add(n);
		}
	}

	@Override
	public void printBoard() {
		System.out.printf("This is the %s!\nAll notes are colored here!\n", getName());
		super.printBoard();

	}

}
