import java.util.ArrayList;
import java.util.Scanner;

import boards.Archive;
import boards.AttachmentBoard;
import boards.PriorityBoard;
import boards.ToDoBoard;
import boards.Trash;
import boards.base.Board;
import notes.AudioNote;
import notes.DocumentNote;
import notes.ImageNote;
import notes.Note;
import notes.PriorityNote;
import notes.TodoNote;

public class CleanNote {

	public static final int NUMBER_OF_BOARDS = 6;

	public static void main(String[] args) {

		System.out.println("Initializing note boards . . .");
		ArrayList<Board> allBoards = new ArrayList<Board>(6);
		Board textBoard = new Board("Text Board");
		Board todoBoard = new ToDoBoard("TODO Board");
		Board attachBoard = new AttachmentBoard("ATTATCHMENT Board");
		Board priorityBoard = new PriorityBoard("PRIORITY Board");
		Board archive = new Archive("ARCHIVE");
		Board trash = new Trash("Trash");

		allBoards.add(textBoard);
		allBoards.add(todoBoard);
		allBoards.add(priorityBoard);
		allBoards.add(attachBoard);
		allBoards.add(archive);
		allBoards.add(trash);

		System.out.println("Initializing complete.\n");

		// MENU
		Scanner in = new Scanner(System.in);
		String humanInput = "";
		int choice = 1;
		System.out.println("WELCOME to CLEAN NOTE!");
		while (choice != 0) {
			System.out.println("\nPlease choose an option from the menu.");
			System.out.println("[1] Create new *NOTE*.");
			System.out.println("[2] Display all NOTES.");
			System.out.println("[3] Display BOARDS.");
			System.out.println("[4] SELECT NOTE by giving ID.");
			System.out.println("[0] EXIT program.");

			choice = in.nextInt();
			in.nextLine();

			switch (choice) {
			case 1:
				System.out.println("What kind of NOTE?");
				System.out.println("[1] TEXT NOTE.");
				System.out.println("[2] TODO NOTE.");
				System.out.println("[3] PRIORITY NOTE.");
				System.out.println("[4] ATTACHMENT NOTE.");
				System.out.println("[5] GO BACK.");

				choice = in.nextInt();
				in.nextLine();

				Note note = null;

				if (choice == 1) {
					note = new Note();

				} else if (choice == 2) {
					note = new TodoNote();

				} else if (choice == 3) {
					note = new PriorityNote();

				} else if (choice == 4) {
					System.out.println("[1] New IMG NOTE.");
					System.out.println("[2] New DOC NOTE.");
					System.out.println("[3] New AUDIO NOTE.");
					choice = in.nextInt();
					in.nextLine();
					if (choice == 1) {
						note = new ImageNote();

					} else if (choice == 2) {
						note = new DocumentNote();

					} else if (choice == 3) {
						note = new AudioNote();

					} else {
						break;
					}
				} else {
					break;
				}

				if (note != null && !note.isEmpty()) {

					for (Board b : allBoards) {
						if (b instanceof Archive || b instanceof Trash)
							continue;
						b.addToBoard(note);
					}
				}
				break;
			case 2:
				for (Board b : allBoards) {
					if (b instanceof Archive || b instanceof Trash)
						continue;
					for (Note n : b.getNotesInBoard()) {
						n.printNote();
					}
				}
				break;
			case 3:
				int num = 1;
				for (Board b : allBoards) {
					System.out.println("[" + num + "]" + " " + b.getName());
					num++;
				}

				choice = in.nextInt();
				switch (choice) {
				case 1:
					textBoard.printBoard();
					break;
				case 2:
					todoBoard.printBoard();
					break;
				case 3:
					priorityBoard.printBoard();
					break;
				case 4:
					attachBoard.printBoard();
					break;
				case 5:
					archive.printBoard();
					break;
				case 6:
					trash.printBoard();
					break;
				}
				break;
			case 4: // select by id
				System.out.print("Enter NOTE ID: ");
				humanInput = in.nextLine();
				Note currentNote = null;
				for (Board b : allBoards) {
					if (currentNote != null)
						break;
					currentNote = b.getById(humanInput);
				}

				if (currentNote != null) {
					System.out.println("\nSelection: ");
					currentNote.printNote();
					System.out.println("[1] DELETE NOTE.");
					System.out.println("[2] ARCHIVE NOTE.");
					System.out.println("[3] EDIT NOTE");
					choice = in.nextInt();
					in.nextLine();
					switch (choice) {
					case 1:
						for (Board b : allBoards) {
							if (!b.getNotesInBoard().contains(currentNote)) {
								if (b instanceof Trash)
									((Trash) b).addToBoard(currentNote);
								continue;
							}

							if (b instanceof Archive) {
								System.out.println("Delete ARCHIVE entry too? y/n");
								humanInput = in.nextLine();
								if (humanInput.equals("n"))
									continue;
							}
							b.getNotesInBoard().remove(currentNote);
						}
						System.out.println("Note moved to TRASH.");
						break;
					case 2:
						for (Board b : allBoards) {
							if (b instanceof Archive)
								((Archive) b).addToBoard(currentNote);
						}
						System.out.println("Note archived.");
						break;
					case 3:
						if (currentNote instanceof TodoNote) {
							System.out.println("\nSelection: ");
							currentNote.printNote();

							System.out.println("[1] MARK COMPLETE");
							choice = in.nextInt();
							in.nextLine();
							if (choice == 1) {
								System.out.println("Enter position to mark as complete");
								choice = in.nextInt();
								in.nextLine();
								((TodoNote) currentNote).markTodoComplete(choice);

							}
						}
						break;
					}
				} else {
					System.out.println("Note not found.");
				}
				break;
			}

		}
		System.out.println("See you next time!");
	}

}
