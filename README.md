Clean Note
======================

Created by team Thanos 
----------------------

Alexander Rusev(nekr0s), Nikola Bunis(NikolaBunis), Todor Slavov(todorslavov72)


### Purpose

Clean Note is a note taking application that allows you to create and organise various types of notes and interact with them.

It has been created as a group project assignment in object oriented programming for our Java+Android Telerik Academy Alpha course.

### Repository URL

https://gitlab.com/nekr0s/clean-note.git

### Class Diagrams

[Class Diagrams Repository](https://gitlab.com/nekr0s/clean-note/tree/master/diagrams)

 * Simplified class diаgram

![Simplified Class Diagram](diagrams/noteAndBoardDiagram.png "Simplified Class Diagram")



**Created by team Thanos, 2018**